import React, { Component } from "react";

class Message extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ""
    };
  }

  handleChange = e => {
    this.setState({ message: e.target.value });
  };

  handleClick = e => {
    this.props.sendMessage(this.state.message);
  };

  render() {
    return (
      <div>
        <input
          style={{ height: "3em", width: "20em" }}
          placeholder="Type your message here"
          onChange={this.handleChange}
        />
        <input type="button" value="Send" onClick={this.handleClick} />
      </div>
    );
  }
}

export default Message;
