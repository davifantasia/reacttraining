import React from "react";

function Conversation(props) {
  return (
    <div>
      {props.messages.map(message => (
        <p>{message}</p>
      ))}
    </div>
  );
}

export default Conversation;
