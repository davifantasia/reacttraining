import React from "react";
import Message from "../src/components/message";
import Conversation from "../src/components/conversation";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: []
    };
  }

  sendMessage = message => {
    const { messages } = this.state;
    messages.push(message);
    this.setState({ messages });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">Message</header>
        <Message sendMessage={this.sendMessage} />
        <Conversation messages={this.state.messages} />
      </div>
    );
  }
}

export default App;
